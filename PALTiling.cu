#include <cstring>
using std::memcpy;

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <list>

#include <limits>
#include <cassert>

#include "CSalt.hpp"
#include "algo.hpp"

int main(int argc, char** argv){
	if(argc%2==0){
		std::cout << "usage: " << argv[0] << " --opt1 val1 --opt2 val2 .. --optn valn" << std::endl;
		return 500;
	}

	std::string filename = "";
	std::string Xfilename = "";
	std::string Yfilename = "";

	std::string csalt = "CSalt";
	std::string algoname = csalt;

	size_t* mA;

	size_t rInc = 10;
	size_t device = 0;
	size_t maxk = 0;
	size_t maxr = 200;
	float thresinc = 0.05;
	float eps = 1.0;
	float lam = 2;
	float delta = 0.5;
	char sep = '\t';
	
	size_t c,m,n;
	float *D = 0;

	size_t showObj = 0; //0: no output, 1:show function values, 2: display matrices

	for(size_t i=1; i<argc; i+=2){
		std::string opt(argv[i]);
		std::string val(argv[i+1]);

		if(!opt.compare("--rInc")) rInc = get<size_t>(val);
		else if(!opt.compare("--K")) maxk = get<size_t>(val);
		else if(!opt.compare("--D")) filename = val;
		else if(!opt.compare("--X")) Xfilename = val;
		else if(!opt.compare("--Y")) Yfilename = val;
		else if(!opt.compare("--mA")){
			std::vector<size_t> *_mA = new std::vector<size_t>();
			std::stringstream str(val);
			while(!str.eof()){
				std::string ma;
				getline(str,ma,',');
				if(ma.size()>0)
					_mA->push_back(get<size_t>(ma));
			}
			mA=_mA->data();
			c=_mA->size();
		}else if(!opt.compare("--c")) c = get<size_t>(val);
		else if(!opt.compare("--A")) algoname = val;
		else if(!opt.compare("--maxR")) maxr = get<size_t>(val);
		else if(!opt.compare("--eps")) eps = get<float>(val);
		else if(!opt.compare("--delta")) delta = get<float>(val);
		//else if(!opt.compare("--mu")) mu = get<float>(val);
		else if(!opt.compare("--lam")) lam = get<float>(val);
		else if(!opt.compare("--dev")) device = get<size_t>(val);
		else if(!opt.compare("--t")) thresinc = get<float>(val);
		else if(!opt.compare("--sep")) sep = get<char>(val);
		else if(!opt.compare("--showobj")) showObj = get<size_t>(val);
		else{
			std::cout << "UNKNOWN OPTION: " << opt << std::endl;
			return 700;
		}
	}

	if(!filename.size() ){
		std::cout << "please provide D, X and Y matrices" << std::endl;
		return 300;
	}

	std::cout << "D\t" << filename << std::endl;
	std::cout << "ALGO\t" << algoname << std::endl;
	std::cout << "ITERS\t" << maxk << std::endl;
	std::cout << "RANKINC\t" << rInc << std::endl;
	std::cout << "THRESINC\t" << thresinc << std::endl;

	cudaDeviceProp dev;
	cudaGetDeviceProperties(&dev, device);
	cudaSetDevice(device);
	std::cout << "GPU\t" << dev.name << std::endl;

	const char pipe[4] = { '-', '\\', '|', '/' };
	
	double start = getTime();
	Matrix::read(filename, D, n, m, sep);
	std::cout << "READ DATA TOOK " << getTime()-start << "s" << std::endl;
	MF<float>* algo;

	if(!algoname.compare(csalt)){
		std::cout << "C\t" << c << std::endl;
		std::cout << "MA\t";
		Matrix::print(mA,1,c);
		algo = new CSalt<float>(D, mA, m, n, rInc, c, lam, eps);
	} else {	
		std::cout << "UNKNOWN ALGORITHM: " << algoname << std::endl;
		std::cout << "CURRENTLY ONLY " << csalt << " IS IMPLEMENTED" << std::endl;
		return 700;
	}

	
	  /////////////////////
	 // ALGORITHM BODY  //
	/////////////////////
	float b, x, y, rEst;
	size_t r = rInc;
	int increased =1;
	start = getTime();
	float oldVal=std::numeric_limits<float>::max();
	while(increased){
		//Do Optimization 
		std::list<float> L;
		float SUM = 0;

		for(size_t k=0; k<maxk; ++k){
			algo->iterate();
			if(showObj==1) {
				float val = algo->objective_relaxed();

				if(k>0){
					L.push_back(abs(oldVal-val));
					//SUM += L.back();
					if(L.size()>500){
						//SUM -= L.front();
						L.pop_front();
					}
					SUM = 0;
					for(auto x : L)
						SUM += x;

					std::cout << std::fixed << std::setprecision(8);
					std::cout << "\r"<< k << "   " << val << "   " << oldVal-val << "   " << SUM/L.size() << "           " << std::flush;
				if(SUM/L.size()<delta)
					break;
				}
				oldVal = val;
			}
			else std::cout << "\rWORK\t" << pipe[k%4] << " (" << k << ")" << std::flush;
		}
		//Compare rank
		algo->optThreshold(thresinc, b, x, y, rEst);
		std::cout << "rEst:" << rEst << " r:"<< r << std::endl;
		//algo->round(x, y);
		if(showObj>1) algo->printMatrices();
		if(rEst<r-1 || r>maxr){
			increased=0;
		}else{
			algo->incrRank(rInc);
			r+=rInc;
			oldVal = algo->objective_relaxed();
		}
	}
	std::cout << "\rWORK\tDONE!" << std::endl;
	algo->round(x, y);
	algo->getRank(rEst);
	if(showObj>1) {
		algo->printMatrices();
		std::cout << "(x,y)=("<<x<<","<<y<<")" << std::endl;
	}

	std::stringstream dfs;
	dfs <<  filename << algoname << ".K" << maxk;
	std::string fn = dfs.str();

	algo->writeOutput(fn);

	std::cout << "ESTIMATED RANK: " << rEst << std::endl;
	std::cout << "WRITE TO: " << fn << std::endl;
	std::cout << "RANK ESTIMATION TOOK " << getTime()-start << "s" << std::endl;

	delete algo;
	delete[] D;

	return r;
}
