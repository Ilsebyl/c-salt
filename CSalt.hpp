#include <iostream>
#include <fstream>
#include <limits>
#include <list>
#include <vector>

#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <thrust/device_vector.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>

#include <math.h>
#include <time.h>
#include <cassert>
#include "algo.hpp"


template <class T>
class CSalt: public MF<T>{
	private:
		using MF<T>::n;
		using MF<T>::m;
		using MF<T>::mA;
		using MF<T>::r;
		using MF<T>::c;
		using MF<T>::D;
		using MF<T>::X;
		using MF<T>::V;
		using MF<T>::Y;
		using MF<T>::C;
		using MF<T>::nOnes;
		using MF<T>::mOnes;
		using MF<T>::u;
		using MF<T>::handle;

		/////////////////
		// Helper
		/////////////////
		T* A;
		T* B;
		T* U; //V
		T* R;
		T* q;
		/////////////////

		/////////////////
		// Constants
		/////////////////
		T* stC;
		T* rOnes;
		/////////////////

		const T mu;
		const T gamma;
		const T eps;
		const T lam;

		void stdCompressionSize(T*& _D){
			T *hstC;
			Matrix::alloc(hstC, this->n, 1);
			T N = 0;
			for(size_t j=0; j<m; ++j){
				for(size_t i=0; i<n; ++i){
					T v = _D[j*n+i];
					hstC[i] += v;
					N += v;
				}
			}
			for(size_t i=0; i<n; ++i){
				T v = hstC[i] / N; 
				assert(v==v);
				if(v>0)
					hstC[i] = -log(v);
				else
					hstC[i] = -log(1.0/N);
			}
			cudaMemcpy(stC, hstC, n*sizeof(T), cudaMemcpyHostToDevice);
			
			delete[] hstC;
		}

	public:
		using MF<T>::getRank;
		using MF<T>::valBinaryPenalizer;

		CSalt(T*& _D, size_t*& _mA, const size_t& _m, const size_t& _n, const size_t& _r, const size_t& _c, const T& _lam = 0.1, const T& _eps = 1.0) : MF<T>(_D,_mA,_m,_n,_r,_c), mu(2*(1+log(n))), gamma(T(1.000001)), lam(_lam), eps(_eps){
			T *hrOnes;
			Matrix::alloc(hrOnes, r, 1, 1);

			checkCudaErrors(cudaMalloc((void **) &A, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &B, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &U, r*c*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &R, r*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &q, n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &stC, n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &rOnes, r*sizeof(T)));

			cudaMemcpy(rOnes, hrOnes, r*sizeof(T), cudaMemcpyHostToDevice);
			stdCompressionSize(_D);
			delete[] hrOnes;
		}

		~CSalt(){
			cudaFree(A);
			cudaFree(B);
			cudaFree(U);
			cudaFree(R);
			cudaFree(stC);
			cudaFree(q);
			cudaFree(rOnes);
		}

		void handleRankInc(){
			cudaFree(A);
			cudaFree(B);
			cudaFree(U);
			cudaFree(R);
			cudaFree(rOnes);

			T *hrOnes;
			Matrix::alloc(hrOnes, r, 1, 1);

			checkCudaErrors(cudaMalloc((void **) &A, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &B, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &U, r*c*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &R, r*r*sizeof(T)));

			checkCudaErrors(cudaMalloc((void **) &rOnes, r*sizeof(T)));
			cudaMemcpy(rOnes, hrOnes, r*sizeof(T), cudaMemcpyHostToDevice);

			delete[] hrOnes;
		}

		T getEps() const {
			return eps;
		}

		T objective_relaxed() const {
			const T x = 1.0, z = 0.0;
			T loss = 0;

			MF<T>::compNoiseRelaxed();
			cublasSnrm2(handle, m*n, C, 1, &loss);	
			//printf("loss = %f\n",loss);
			loss = mu*0.5*loss*loss;
			
			//regularizer G
			T Ysum = 0;
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			cudaMemcpy(u, rOnes, r*sizeof(T), cudaMemcpyDeviceToDevice);
			cublasSgemv(handle, CUBLAS_OP_N, r, m, &x, Y, r, mOnes, 1, &eps, u, 1);
			thrust::device_ptr<T> uT(u);
			thrust::plus<T> pl;
			const T comp = thrust::transform_reduce(uT, uT+r, entropyfunc(log(Ysum+r*eps)), z, pl);

			T stdCsum = 0;
			cublasSgemv(handle, CUBLAS_OP_T, n, r, &x, X, n, stC, 1, &z, u, 1);
			for(int a=0;a<c;a++){
				cublasSgemv(handle, CUBLAS_OP_T, n, r, &x, V+a*r*n, n, stC, 1, &x, u, 1);	
			}
			cublasSasum(handle, r, u, 1, &stdCsum);			

			return loss + comp + stdCsum + Ysum + specReg_relaxed()+lam*valBinaryPenalizer();
		}

		T objective() const {
			const T x = 1.0, z = 0.0;

			T Nsum = 0;
			MF<T>::compNoiseBinary();
			cublasSasum(handle, m*n, C, 1, &Nsum);

			T Ysum = 0;
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			thrust::plus<T> pl;
			cublasSgemv(handle, CUBLAS_OP_N, r, m, &x, Y, r, mOnes, 1, &z, u, 1);
			thrust::device_ptr<T> uT(u);
			const T entY = thrust::transform_reduce(uT, uT+r, entropyfunc(log(Ysum+Nsum)), T(0), pl);

			thrust::device_ptr<T> CT(C);
			thrust::transform(CT, CT+(m*n), CT, absfunc());
			cublasSgemv(handle, CUBLAS_OP_N, n, m, &x, C, n, mOnes, 1, &z, q, 1);
			thrust::device_ptr<T> qT(q);
			const T entN = thrust::transform_reduce(qT, qT+n, entropyfunc(log(Ysum+Nsum)), T(0),pl);

			T stdCsum = 0;
			cublasSgemv(handle, CUBLAS_OP_T, n, r, &x, X, n, stC, 1, &z, u, 1);
			for(int a=0;a<c;a++){
				cublasSgemv(handle, CUBLAS_OP_T, n, r, &x, V+a*r*n, n, stC, 1, &x, u, 1);	
			}
			
			cublasSasum(handle, r, u, 1, &stdCsum);

			T Ncsum = 0;
			thrust::transform(qT, qT+n, qT, theta<T>(0));
			cublasSdot(handle, n, q, 1, stC, 1, &Ncsum);

			return entY + entN + stdCsum + Ncsum + specReg();
		}

		//If relaxed function is calculated we can use A to store the result 
		T specReg_relaxed() const {
			const T x=1.0, y2=-2.0, z=0.0;
			T val=0.0, val_a =0.0;
			//Compute sum_a tr((YD'+Ya(1-Da'))Va)
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, m, &x, D, n, Y, r, &z, A, n);
			//First sum_a tr(YD'Va)
			for (int a=0;a<c;a++){
				T vFac = 1.0;//*(mA+a)/m;
				cublasSdot(handle, r*n, A, 1, V+a*r*n, 1, &val_a);
				val+=vFac*val_a;
			}
			//Second sum_a tr(Ya(1-Da')Va)
			int sumMA=0;
			for (int a=0;a<c;a++){
				cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, *(mA+a), &y2, D+n*sumMA, n, Y+r*sumMA, r, &z, A, n);
				cublasSgemv(handle, CUBLAS_OP_N, r, *(mA+a), &x, Y+r*sumMA, r, mOnes, 1, &z, u, 1);
				cublasSger(handle, n, r, &x, nOnes, 1, u, 1, A, n);
				cublasSdot(handle, r*n, A, 1, V+a*r*n, 1, &val_a);
				val+=val_a;
				sumMA+=*(mA+a);
			}
			return val;
		}

		//If concrete function is calculated we can use X to store intermediate matrices,
		//the original value of X is not required!
		T specReg() const {
			const T x=1.0, y2 = -2.0, z=0.0;
			T val=0.0, val_a =0.0;
			//Compute sum_a tr((YD'+Ya(1-Da'))Va)
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, m, &x, D, n, Y, r, &z, X, n);
			//First sum_a tr(YD'Va)
			for (int a=0;a<c;a++){
				T vFac = 1.0;//*(mA+a)/m;
				cublasSdot(handle, r*n, X, 1, V+a*r*n, 1, &val_a);
				val+=vFac*val_a;
			}
			//Second sum_a tr(Ya(1-2Da')Va)
			int sumMA=0;
			for (int a=0;a<c;a++){
				T vFac = 1.0;//*(mA+a)/m;
				cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, *(mA+a), &y2, D+n*sumMA, n, Y+r*sumMA, r, &z, X, n);
				cublasSgemv(handle, CUBLAS_OP_N, r, *(mA+a), &x, Y+r*sumMA, r, mOnes, 1, &z, u, 1);
				cublasSger(handle, n, r, &x, nOnes, 1, u, 1, X, n);
				cublasSdot(handle, r*n, X, 1, V+a*r*n, 1, &val_a);
				val+=vFac*val_a;
				sumMA+=*(mA+a);
			}
			return val;
		}

		//If relaxed function is calculated we can use A to store the result 
		T redundancyVReg_relaxed() const {
			const T y = -1.0;
			//Compute Hadamard product (1-X) o V1 o..o Vc in A
			thrust::multiplies<T> mul;
			cudaMemcpy(A, X, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cublasSger(handle, n, r, &y, nOnes, 1,  rOnes, 1, A, n);
			thrust::device_ptr<T> VT(V);
			thrust::device_ptr<T> AT(A);
			for (int a=0;a<c;a++){
				thrust::transform(VT+a*r*n, VT+(a+1)*r*n, AT, AT, mul);
			}
			T sumXVa = 0;
			cublasSasum(handle, n*r, A, 1, &sumXVa);
			return sumXVa;
		}

		// If concrete function is calculated we can use X to store the Hadamard product, 
		// the original value of X is lost! 
		T redundancyVReg() const {
			const T y = -1.0;
			//Compute Hadamard product (1-X) o V1 o..o Vc in A
			thrust::multiplies<T> mul;
			cublasSger(handle, n, r, &y, nOnes, 1,  rOnes, 1, X, n);
			thrust::device_ptr<T> VT(V);
			thrust::device_ptr<T> XT(X);
			for (int a=0;a<c;a++){
				thrust::transform(VT+a*r*n, VT+(a+1)*r*n, XT, XT, mul);
			}
			T sumXVa = 0;
			cublasSasum(handle, n*r, X, 1, &sumXVa);
			return sumXVa;
		}

		// X is n x r
		// Y is r x m
		void iterateX(){
			const T x = 1.0, z = 0.0, v=0.0; 

			////////////////////////////////
			// UPDATE X
			////////////////////////////////

			MF<T>::compNoiseRelaxed();
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, m, &mu, C, n, Y, r, &v, A, n);
			cublasSger(handle, n, r, &x, stC, 1, rOnes, 1, A, n);			

			//Compute stepsize
			T alpha = 0;
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, r, r, m, &x, Y, r, Y, r, &z, R, r);
			cublasSnrm2(handle, r*r, R, 1, &alpha);
			alpha *= mu*gamma;
			alpha = -1/alpha;

			//Do the step
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, r, &x, X, n, &alpha, A, n, X, n);

			thrust::device_ptr<T> XT(X);
			thrust::transform(XT, XT+(r*n), XT, proxDelta(-1.0*alpha*lam));	
		}

		// X is n x r
		// Y is r x m
		void iterateV(){
			const T x = 1.0, z = 0.0, v=0.0; //v=-vFac

			////////////////////////////////
			// UPDATE V
			////////////////////////////////

			int sumMA=0;
			T alpha=0;
			thrust::device_ptr<T> VT(V);
			thrust::device_ptr<T> AT(A);
			MF<T>::compNoiseRelaxed();
			for(int a=0;a<c;a++){
				//Compute Gradient in A
				cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, *(mA+a), &mu, C+n*sumMA, n, Y+r*sumMA, r, &v, A, n);
				cublasSger(handle, n, r, &x, stC, 1, rOnes, 1, A, n);
				addGradVSpec(a, sumMA);

				//Compute stepsize
				alpha = 0;
				cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, r, r, *(mA+a), &x, Y+r*sumMA, r, Y+r*sumMA, r, &z, R, r);
				cublasSnrm2(handle, r*r, R, 1, &alpha);
				alpha *= mu*gamma;
				alpha = -1/alpha;

				//Do the step
				cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, r, &x, V+a*r*n, n, &alpha, A, n, V+a*r*n, n);
				thrust::transform(VT+a*r*n, VT+((a+1)*r*n), VT+a*r*n, proxDelta(-1.0*alpha*lam));
				sumMA+=*(mA+a);
			}
		}

		// X is n x r
		// Y is r x m
		void iterateY(){
			const T x = 1.0, z = 0.0;
			////////////////////////////////
			// UPDATE Y
			////////////////////////////////

			//Compute Gradient in B
			int sumMA=0;
			MF<T>::compNoiseRelaxed();
			cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, m, n, &mu, X, n, C, n, &z, B, r);
			for(int a=0;a<c;a++){
				cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, *(mA+a), n, &mu, V+r*n*a, n, C+n*sumMA, n, &x, B+r*sumMA, r);
				sumMA+=*(mA+a);
			}
			addGradYSpec();		

			T Ysum = 0;
			cublasSasum(handle, m*r, Y, 1, &Ysum);
			cudaMemcpy(u, rOnes, r*sizeof(T), cudaMemcpyDeviceToDevice);
			cublasSgemv(handle, CUBLAS_OP_N, r, m, &x, Y, r, mOnes, 1, &eps, u, 1);
			thrust::device_ptr<T> uT(u);
			thrust::transform(uT, uT+r, uT, derivYfunc(Ysum+r*eps));
			cublasSger(handle, r, m, &x, u, 1, mOnes, 1, B, r);
			cublasSger(handle, r, m, &x, rOnes, 1, mOnes, 1, B, r);

			//Compute stepsize
			T beta = 0, beta_a=0;
			for(int a=0;a<c;a++){
				cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, r, &x, X, n, &x, V+a*r*n, n, A, n);
				cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, r, n, &x, A, n, A, n, &z, R, r);
				cublasSnrm2(handle, r*r, R, 1, &beta_a); 
				beta += beta_a;
			}
			beta= mu*beta + 2*m*(1-1/r)/eps; //Lipschitz constant of muF + Leps + |Y|
			beta = -1/(gamma*beta);

			//Do the step
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, r, m, &x, Y, r, &beta, B, r, Y, r);

			thrust::device_ptr<T> YT(Y);
			thrust::transform(YT, YT+(r*m), YT, proxDelta(-1.0*beta*lam));
		}

		void addGradYSpec(){
			const T x = 1.0, z = 0.0;
			//A=sum_a Va
			cudaMemcpy(A, V, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
			for(int a=1;a<c;a++){
				cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, r, &x, V+a*n*r, n, &x, A, n, A, n);
			}
			//Ba=vFac*A'Da +vFac*Va(1-2Da)
			int sumMA=0;
			for(int a=0;a<c;a++){
				T y2=-2.0;
				cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, *(mA+a), n, &x, A, n, D+n*sumMA, n, &x, B+r*sumMA, r); 
				cublasSgemv(handle, CUBLAS_OP_T, n, r, &x, V+r*n*a, n, nOnes, 1, &z, u, 1);
				cublasSger(handle, r, *(mA+a), &x, u, 1, mOnes, 1, B+r*sumMA, r);
				cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, r, *(mA+a), n, &y2, V+a*n*r, n, D+n*sumMA, n, &x, B+r*sumMA, r);
				sumMA+=*(mA+a);
			}
		}

		void addGradVSpec(int a, int sumMA){
			T vFac = 1.0;
			const T x = 1.0, z = 0.0, y2=-2.0*vFac;
			//A=DY'
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, m, &vFac, D, n, Y, r, &x, A, n);
			//A = A -2DaYa
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, n, r, *(mA+a), &y2, D+n*sumMA, n, Y+r*sumMA, r, &x, A, n);
			//A = A + 1(|Ya1| .. |Yar|)
			cublasSgemv(handle, CUBLAS_OP_N, r, *(mA+a), &x, Y+r*sumMA, r, mOnes, 1, &z, u, 1);
			cublasSger(handle, n, r, &vFac, nOnes, 1, u, 1, A, n);
			
		}


		void iterate(){
			iterateY();
			iterateX();
			iterateV();
		}

		void optThreshold(const T& thres, T& _best, T& _x, T& _y, T& _r) {
			cudaMemcpy(A, X, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cudaMemcpy(U, V, r*c*n*sizeof(T), cudaMemcpyDeviceToDevice);
			cudaMemcpy(B, Y, m*r*sizeof(T), cudaMemcpyDeviceToDevice);

			_best = std::numeric_limits<T>::max();
			T rAct =-10;

			for(T x=1.0; x>=0.0; x-=thres){
				for(T y=1.0; y>=0.0; y-=thres){
					
					this->round(x, y);
					getRank(rAct);
		
					//MF<T>::printMatrices();
					const T val = objective();
					//printf("Round at (%f,%f),  F(X,Y,V)= %f\n",x,y, val);
					if(val<_best){
						_best = val;
						_x = x;
						_y = y;
						_r = rAct;
					}

					cudaMemcpy(X, A, r*n*sizeof(T), cudaMemcpyDeviceToDevice);
					cudaMemcpy(V, U, r*c*n*sizeof(T), cudaMemcpyDeviceToDevice);
					cudaMemcpy(Y, B, m*r*sizeof(T), cudaMemcpyDeviceToDevice);
				}
			}
		}

		struct derivYfunc{
			const T lYspre;

			derivYfunc(T _Yspre) : lYspre(log(_Yspre)) {}

			__host__ __device__
			T operator()(const T& x) const { 
				return -log(x) + lYspre;
			}
		};

		struct entropyfunc{
			const T lYspre;

			entropyfunc(T _Yspre) : lYspre(_Yspre) {}

			__host__ __device__
			T operator()(const T& x) const { 
				if(x==T(0)) return 0;
				return -(x+1) * (log(x)-lYspre);
			}
		};

		struct absfunc{
			__host__ __device__
			T operator()(const T& x) const {
				return abs(x);
			}
		};

		struct proxDelta{
			const T a;

			proxDelta(T _a) : a(_a){}

			__host__ __device__
			T operator()(const T& z) const {
				return (z<=0.5) ? max(T(0), z-a) : min(T(1), z+a);
			}
		};
};
