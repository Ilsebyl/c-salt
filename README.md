# C-SALT #

### How do I get set up? ###

Run the "build" script to build C-Salt. You might have to adjust the build command to the properties of your GPU or C++ libraries which are installed.
The runTest script invokes one of the real-world experiments according to your specifications. Simply run one of the following commands:

```
./runTest NewsPoliticsDTmA936,775 1
./runTest NewsSpaceRelDTmA622,980 1
./runTest MovieRecDTmA998,997 1

```

The last argument 1 indicates the GPU device number, so you might want to adjust this one.

For C-Salt, you can specify the following parameters:

* --rInc: the rank increment (default: 10)
* --K: the number of iterations (default: 1000)
* --D: the path to the (transposed) data matrix, i.e., the (n x m) data matrix where m is the number of transactions
* --maxR: maximum rank to consider (default: 200)
* --dev: number of gpu device (default: 0)
* --t: the threshold increment at which matrices are rounded to binary values in the end (default: 0.05)
* --sep: the separator used in the table of D (default: '\t') 

The Julia scripts in the folder SCRIPTS can be used to generate synthetic data matrices and to evaluate the results obtained by the above commands. You can run these scripts without installation with [JuliaBox](https://www.juliabox.org/ "juliabox").

Have a good C-Salt treatment!

### Who do I talk to? ###

* Sibylle Hess (sibylle.hess@tu-dortmund.de)