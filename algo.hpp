#ifndef __ALGO__
#define __ALGO__
#define checkCudaErrors(x) x

#include <sys/time.h>

inline double getTime(){
	struct timeval tv;
	if (gettimeofday(&tv, NULL)==0)
		return tv.tv_sec+((double)(tv.tv_usec))/1e6;
	else
		return 0.0;
}

template <class T>
T get(std::string s){
	std::stringstream is(s);
	T t;
	is >> t;
	return t;
}

namespace Matrix{
	template <class T>
	void alloc(T*& A, const size_t& m, const size_t& n, const size_t& val = 0){
		A = new T[m*n];
		for(size_t i=0; i<m; ++i){
			for(size_t j=0; j<n; ++j)
				A[i*n+j] = val;
		}
	}

	template <class T>
	void print(T*& A, const size_t& m, const size_t& n, std::ostream& out = std::cout){
		out << std::fixed << std::setprecision(2);
		for(size_t j=0; j<m; ++j){
			for(size_t i=0; i<n; ++i)
				out << A[i*m+j] << ' ';
			out << std::endl;
		}
	}

	template <class T>
	void printGPU(T* A, const size_t& m, const size_t& n){
		T* hA;
		alloc(hA, m, n);
		cudaMemcpy(hA, A, m*n*sizeof(T), cudaMemcpyDeviceToHost);

		print(hA, m, n);

		delete hA;
	}

	template <class T>
	void writeGPU(std::string filename, T* A, const size_t& m, const size_t& n){
		T* hA;
		alloc(hA, m, n);
		cudaMemcpy(hA, A, m*n*sizeof(T), cudaMemcpyDeviceToHost);
		std::ofstream outfile(filename, std::ios::out | std::ios::binary);
		outfile.write((char*)hA, m*n*sizeof(T));
		outfile.close();
		delete hA;
	}

	template <class T>
	void read(std::string filename, T*& D, size_t& m, size_t& n, const char& sep){
		std::vector<std::vector<T>*>* indata = new std::vector<std::vector<T>*>();
		std::string line;

		std::ifstream datafile(filename);
		while(!datafile.eof()){
			getline(datafile, line);
			if(line.size()>1){
				std::vector<T>* rowlist = new std::vector<T>();

				std::stringstream ls(line);
				std::string vs;
				while(!ls.eof()){
					getline(ls, vs, sep);
					T val = get<T>(vs);
					rowlist->push_back(val);
				}

				indata->push_back(rowlist);
			}
			std::cout << '\r' << indata->size() << std::flush;
		}
		datafile.close();
		std::cout << std::endl;

		m = indata->size();
		n = indata->front()->size();

		Matrix::alloc(D, m, n);

		int i=0, j=0;
		for(std::vector<T>* l : *indata){
			j=0;
			for(auto x : *l){
				D[j*m+i] = x;
				++j;
			}
			++i;
			delete l;
		}
		delete indata;
	}

	template <class T>
	void read(std::string filename, T*& A, const size_t& m, const size_t& n){
		std::ifstream infile(filename, std::ios::in | std::ios::binary);
		infile.read((char*)A, m*n*sizeof(T));
		infile.close();
	}

	template <class T>
	void readGPU(std::string filename, T* A, const size_t& m, const size_t& n){
		T* hA;
		alloc(hA, m, n);
		std::ifstream infile(filename, std::ios::in | std::ios::binary);
		infile.read((char*)hA, m*n*sizeof(T));
		infile.close();
		cudaMemcpy(A, hA, m*n*sizeof(T), cudaMemcpyHostToDevice);
		delete hA;
	}

	template <class T>
	void fillI(T*& A, const size_t& m, const size_t& n){
		for(size_t i=0; i<m; ++i){
			for(size_t j=0; j<n; ++j)
				A[i*n+j] = (rand()/T(RAND_MAX));
		}
	}

	template <class T>
	void fillCombinations(T*& A, const size_t& m, const size_t& n){
		for(size_t i=0; i<n; ++i){ //for each class (column)
			int b = pow(2,i);
			int div = 2*b;		
			for(size_t j=0; j<m; ++j){
				A[i*m+j] = (j%div)<b;
			}
		}
	}

	template <class T>
	void fillRand(T*& A, const size_t& m, const size_t& n){
		for(size_t i=0; i<n; ++i){
			for(size_t j=0; j<m; ++j){
					A[i*m+j] = (rand()/T(RAND_MAX));
					//A[i*m+j] = 0.5;
			}
		}
	}
	
	//Copies the mxn upper left tile from B into A
	template <class T>
	void copy(T*& B, const size_t& mA, const size_t& mB, const size_t& nB, T*& A){
		for(size_t j=0; j<mB; ++j){
			for(size_t i=0; i<nB; ++i)
				A[i*mA+j] = B[i*mB+j];
		}
	}
}

template <class T>
struct theta{
	const T a;

	theta(T _a = 0.5) : a(_a){}

	__host__ __device__
	T operator()(const T& z) const {
		return z>a;
	}
};

// the binary penalizing function (only correct for x in [0,1] - 
// after the prox application this is always the case)
template<class T> 
struct lambda{

	__host__ __device__
	T operator()(const T& x) const {
		return -abs(0.5-x)+0.5;
	}
};

template <class T>
class MF{
	protected:
		T* X;
		T* Y;//rxm
		T* V;//nxrc

		/////////////////
		// Helper
		/////////////////
		T* u;
		T* C;
		/////////////////

		/////////////////
		// Constants
		/////////////////
		T* D;
		T* mOnes;
		T* nOnes;
		/////////////////

		size_t* mA; //Array of number of transactions in each class 
		const size_t m;
		const size_t n;
		const size_t c; //Number of classes
		size_t r;

		cublasHandle_t handle;
		cublasStatus_t status;

	public:
		MF(T*& _D, size_t*& _mA, const size_t _m, const size_t& _n, const size_t& _r, const size_t& _c) : m(_m), n(_n), r(_r), c(_c){
			std::cout << "m=" << m << ", n=" << n << std::endl;
			mA=_mA;
			cublasCreate(&handle);
			cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_HOST);

			T *hX, *hY, *hV, *hmOnes, *hnOnes;

			Matrix::alloc(hX, n, r);
			Matrix::alloc(hV, n, r*c);
			Matrix::alloc(hnOnes, n, 1, 1);
			Matrix::alloc(hY, r, m);
			Matrix::alloc(hmOnes, m, 1, 1);

			Matrix::fillRand(hX, n, r);
			Matrix::fillRand(hV, n, r*c);
			Matrix::fillRand(hY, r, m);

			checkCudaErrors(cudaMalloc((void **) &X, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &V, r*c*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &u, r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &nOnes, n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &Y, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &C, m*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &mOnes, m*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &D, m*n*sizeof(T)));

			cudaMemcpy(Y, hY, m*r*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(mOnes, hmOnes, m*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(D, _D, m*n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(nOnes, hnOnes, n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(X, hX, r*n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(V, hV, r*c*n*sizeof(T), cudaMemcpyHostToDevice);

			delete[] hX;
			delete[] hV;
			delete[] hY;
			delete[] hmOnes;
			delete[] hnOnes;
		}

		~MF(){
			cudaFree(X);
			cudaFree(V);
			cudaFree(u);
			cudaFree(nOnes);
			cudaFree(mOnes);
			cudaFree(C);
			cudaFree(D);
			cudaFree(Y);
			cudaDeviceReset();
		}


		virtual T objective() const = 0;

		virtual T objective_relaxed() const = 0;

		virtual void iterate() = 0;

		virtual void handleRankInc() = 0;

		virtual void optThreshold(const T& thres, T& _best, T& _x, T& _y, T& _r) = 0;
		
		//Computes C=XY+(VaYa)a-D
		virtual void compNoiseRelaxed() const {
			const T y = -1.0, x=1.0;
			cudaMemcpy(C, D, m*n*sizeof(T), cudaMemcpyDeviceToDevice);
			int sumMA=0;
			//printf("\nD\n");
			//Matrix::printGPU(C, n, m);
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, m, r, &x, X, n, Y, r, &y, C, n);
			for(int a=0; a<c; a++){
				cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, *(mA+a), r, &x, V+n*r*a, n, Y+r*sumMA, r, &x, C+n*sumMA, n);
				//printf("\nN_%i\n",a);
				//Matrix::printGPU(C+n*sumMA, n, *(mA+a));
				sumMA+=*(mA+a);
			}
		}

		//Computes C=theta(YX)-D
		virtual void compNoiseBinary() const {
			const T x = 1.0, z=0.0, y = -1.0;
			int sumMA=0;
			//C=XY
			cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, m, r, &x, X, n, Y, r, &z, C, n);
			for(int a=0; a<c; a++){
				//C=theta(XY+(VaYa)a)
				//cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, *(mA+a), r, &x, X, n, Y+r*sumMA, r, &z, C+n*sumMA, n);
				cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, *(mA+a), r, &x, V+n*r*a, n, Y+r*sumMA, r, &x, C+n*sumMA, n);
				sumMA+=*(mA+a);
			}
			thrust::device_ptr<T> ptrC(C);
			thrust::transform(ptrC, ptrC+m*n, ptrC, theta<T>(0.5));
			//C=theta(XY+(VaYa)a)-D
			cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, m, &x, C, n, &y, D, n, C, n);
		}

		/**
		* This function is invoked during thresholding, the matrices A, U and B 
		* should therefore not be used. X and V can however be altered to this version. 
		**/
		virtual void getRank(T& rEst) {
			const T w=1.0, x = 1.0, y=-1.0, z = 0.0;
			thrust::device_ptr<T> VT(V);
			//u = X*1
			cublasSgemv(handle, CUBLAS_OP_T, n, r, &x, X, n, nOnes, 1, &z, u, 1);
			for(int a=0;a<c;a++){	
				//Va=theta(Va-X)
				cublasSgeam(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, r, &y, X, n, &x, V+a*n*r, n, V+a*n*r, n);
				thrust::transform(VT+a*n*r, VT+(a+1)*n*r, VT+a*n*r, theta<T>(0.5)); 
				//u += 1/c*Va^T*1
				cublasSgemv(handle, CUBLAS_OP_T, n, r, &w, V+a*n*r, n, nOnes, 1, &x, u, 1);	
			}	
			thrust::device_ptr<T> uT(u);
			thrust::transform(uT, uT+r, uT, theta<T>(1.0)); 
			//Y = diag(u)*Y
			cublasSdgmm(handle, CUBLAS_SIDE_LEFT, r, m, Y, r, u, 1, Y, r);
			//u = Y*1
			cublasSgemv(handle, CUBLAS_OP_N, r, m, &x, Y, r, mOnes, 1, &z, u, 1);
			thrust::device_ptr<T> uT2(u);
			thrust::transform(uT2, uT2+r, uT2, theta<T>(1)); 
			//X = X*diag(u), Y = diag(u)*Y , Va = Va*diag(u)
			cublasSdgmm(handle, CUBLAS_SIDE_RIGHT, n, r, X, n, u, 1, X, n);
			for(int a=0;a<c;a++){	
				cublasSdgmm(handle, CUBLAS_SIDE_RIGHT, n, r, V+a*n*r, n, u, 1, V+a*n*r, n);
			}	
			cublasSdgmm(handle, CUBLAS_SIDE_LEFT, r, m, Y, r, u, 1, Y, r);
			cublasSasum(handle, r, u, 1, &rEst);
		}

		virtual void round(const T& x, const T& y) {
			roundX(x);
			roundY(y);
			roundV(x);
		}

		virtual void writeOutput(std::string fn) {
			Matrix::writeGPU(fn+".X", X, n, r);
			Matrix::writeGPU(fn+".V", V, n, r*c);
			Matrix::writeGPU(fn+".Y", Y, r, m);
		}

		void incrRank(const size_t& rInc){
			T *hXold, *hYold; 
			T *hX, *hY, *hV, *hVa;
			//DEBUG
			//Matrix::printGPU(V, n, c*r);
			
			Matrix::alloc(hX, n, r+rInc);
			Matrix::alloc(hV, n, (r+rInc)*c);
			Matrix::alloc(hY, r+rInc, m);
			Matrix::fillRand(hX, n, r+rInc);
			//Matrix::fillRand(hV, n, (r+rInc)*c);
			Matrix::fillRand(hY, r+rInc, m);
		
			//Insert mined clusters in new matrix on host device
			Matrix::alloc(hXold, n, r);
			Matrix::alloc(hYold, r, m);
			cudaMemcpy(hXold, X, n*r*sizeof(T), cudaMemcpyDeviceToHost);
			cudaMemcpy(hYold, Y, m*r*sizeof(T), cudaMemcpyDeviceToHost);
			Matrix::copy(hXold, n, n, r , hX);
			Matrix::copy(hYold, r+rInc, r, m, hY);
			for(int a=0; a<c; a++){
				hVa = hV + a*n*(r+rInc);
				cudaMemcpy(hXold, V+a*n*r, n*r*sizeof(T), cudaMemcpyDeviceToHost);
				Matrix::copy(hXold, n, n, r , hVa);
			}
			//DEBUG
			//Matrix::printGPU(V, n, c*r);
			r+=rInc;
			cudaFree(X);
			cudaFree(V);
			cudaFree(Y);
			cudaFree(u);

			checkCudaErrors(cudaMalloc((void **) &X, r*n*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &V, r*n*c*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &Y, m*r*sizeof(T)));
			checkCudaErrors(cudaMalloc((void **) &u, r*sizeof(T)));

			cudaMemcpy(X, hX, r*n*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(V, hV, r*n*c*sizeof(T), cudaMemcpyHostToDevice);
			cudaMemcpy(Y, hY, m*r*sizeof(T), cudaMemcpyHostToDevice);
			
			delete[] hX;
			delete[] hV;
			delete[] hY;
			delete[] hXold;
			delete[] hYold;

			handleRankInc();
		}

		T valBinaryPenalizer() const {
			thrust::plus<T> pl;
			thrust::device_ptr<T> ptrX(X);
			T binPenal = thrust::transform_reduce(ptrX, ptrX+r*n, lambda<T>(), T(0),pl); 
			thrust::device_ptr<T> ptrV(V);
			binPenal += thrust::transform_reduce(ptrV, ptrV+r*n*c, lambda<T>(), T(0),pl);
			thrust::device_ptr<T> ptrY(Y);
			binPenal += thrust::transform_reduce(ptrY, ptrY+r*m, lambda<T>(), T(0),pl);
			return binPenal;
		}	

		void roundX(const T& x) {
			thrust::device_ptr<T> XT(X);
			thrust::transform(XT, XT+(r*n), XT, theta<T>(x));
		}

		void roundV(const T& x) {
			thrust::device_ptr<T> VT(V);
			thrust::transform(VT, VT+(r*c*n), VT, theta<T>(x));
		}

		void roundY(const T& y) {
			thrust::device_ptr<T> YT(Y);
			thrust::transform(YT, YT+(r*m), YT, theta<T>(y));
		}
		
		struct absfunc{
			__host__ __device__
			T operator()(const T& x) const {
				return abs(x);
			}
		};

		virtual void printMatrices(){
			std::cout << "\nX: " << std::endl;
			Matrix::printGPU(X, n, r);
			std::cout << "\nV: " << std::endl;
			Matrix::printGPU(V, n, r*c);
			std::cout << "\nY: " << std::endl;
			Matrix::printGPU(Y, r, m);
		}
		
};
#endif
